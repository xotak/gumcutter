# Global variables

$ConfigPath = "./config.json"

# Dependency check

If (-Not (Test-Path '.\bin\gum.exe')) {
  Write-Host "Gum is missing. Please follow the installation instructions" -ForegroundColor Red
  Exit
}

# Add gum to path temporarily for easier use
$env:Path += ";$PWD\bin\"


# Intro

gum style --foreground 34 --border-foreground 34 --border double --align center --width 50 --margin '1 2' --padding '1 4' 'Gumcutter' 'A TUI app to manage your grasscutter installation'

# If config not found
If (-Not (Test-Path $ConfigPath)) {
  # Warn user
  Write-Host "Configuration file not found, performing initial setup." -ForegroundColor Yellow
  # Launch setup script
  Invoke-Expression "./scripts/InitialSetup.ps1"
} else {
  $Config = Get-Content .\config.json | ConvertFrom-JSON
}

$Action = Invoke-Expression "gum choose 'Launch server' 'Manage' --header 'What do you want to do ?'"

if ($Action -eq 'Launch server') {
  Set-Location $Config.GCPath
  Invoke-Expression "java -jar $Config:GCBin"
}