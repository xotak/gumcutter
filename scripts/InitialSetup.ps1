# Creating config object
$Config = New-Object psobject

# Ask if server is installed
$IsInstalled = Invoke-Expression "gum choose 'Yes' 'No' --header='Is Grasscutter installed ?'"

If ($IsInstalled -eq 'Yes') {
    # Ask for the path
    Write-Host "Please input the path to your Grasscutter installation (Ctrl-Shift-V to paste, Cancel to quit)"
    $GCPath = Invoke-Expression "gum input --placeholder 'Type here'"


    # Check if server binary is present in folder
    while (-Not (Test-Path $GCPath/grasscutter-*.jar)) {
        # If user want to cancel setup
        if ($GCPath -eq "Cancel") {
            Write-Host 'Aborting configuration. Bye !'
            Exit
        }
        
        # Retry
        Write-Host "Server executable not found. Make sure you entered the correct path" -ForegroundColor Red  
        Write-Host "Please input the path to your Grasscutter installation (Ctrl-Shift-V to paste, Cancel to quit)"
        $GCPath = Invoke-Expression "gum input --placeholder 'Type here'"
    }

    Write-Host 'Grasscutter detected sucessfully !'
    $GCBin = Invoke-Expression "Get-ChildItem -Name grasscutter-*.jar"
    # Write path to config
    $Config | Add-Member -MemberType NoteProperty -Name GCPath -Value $GCPath
    $Config | Add-Member -MemberType NoteProperty -Name GCBin -Value $GCBin
    # Detect git repo
    if (Test-Path $GCPath/.git) {
        Write-Host 'Git Repository detected, enabling update feature'
        $Config | Add-Member -MemberType NoteProperty -Name Git-Updatable -Value "True"
    }
    else {
        Write-Host 'Git Repository not found, disabling update feature'
        $Config | Add-Member -MemberType NoteProperty -Name Git-Updatable -Value "False"
    }

    
    # Write config to file
    ConvertTo-Json $Config | Out-File -FilePath .\config.json

    Write-Host 'Setup complete ! Please restart gumcutter for changes to apply' -ForegroundColor Green
}
else {
    # Ask if the user wants to install
    $AutomaticInstall = Invoke-Expression "gum choose 'Yes' 'No' --header='Do you want to run the setup script'"
    
    # If OK, warn the user and start install script with admin privileges
    if ($AutomaticInstall -eq 'Yes') {
        Write-Host "Launching setup script. You will be prompted for administrative privileges. The setup script will open in a new window."
        Pause
        # TODO : I'd like to find a way to get a return value out of following line to check if installer have been completed successfully, but can't seem to get any as we're running the process as admin
        Start-Process powershell -Verb runAs -ArgumentList "& $PWD/scripts/Install-Deps.ps1 -WorkDir $PWD" -Wait

        # Refreshing Path, to be sure git and java is detected.
        $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
        
        Write-Host "Installing server. This might take a while depending on your Internet speed and computer power. Please be patient..."

        if ($null -eq (Invoke-Expression git)) {
            Write-Host "Git not found, using zip method instead."
            $Config | Add-Member -MemberType NoteProperty -Name Git-Updatable -Value "False"
            # Downloading server head zip
            Invoke-WebRequest -Uri "https://github.com/Grasscutters/Grasscutter/archive/refs/heads/development.zip" -UseBasicParsing -OutFile "./downloads/grasscutter.zip"
            # Extract zip
            Expand-Archive -Path ".\downloads\grasscutter.zip" -DestinationPath "./grasscutter"
            # Download resources zip
            Invoke-WebRequest -Uri "https://gitlab.com/yukiz" -UseBasicParsing -OutFile "./downloads/resources.zip"
            # Extract resources 
            Expand-Archive -Path ".\downloads\resources.zip" -DestinationPath ".\downloads\"
            # TODO : Copy
            # Changing location to server folder
            Set-Location "./grasscutter"
            # Setup gradlew and compile
            Invoke-Expression './gradlew.bat'
            Invoke-Expression './gradlew-jar.bat'
            # Update config and write to file
            $Config | Add-Member -MemberType NoteProperty -Name GCPath -Value "./grasscutter"
            $Config | Add-Member -MemberType NoteProperty -Name GCBin -Value Invoke-Expression 'Get-ChildItem -Name "grasscutter-*.jar'
            ConvertTo-Json $Config | Out-File -FilePath .\config.json
        } else {
            Write-Host "Git found !"
            $Config | Add-Member -MemberType NoteProperty -Name Git-Updatable -Value "True"
            # Clone Grasscutter repo
            Invoke-Expression "git clone https://github.com/Grasscutters/grasscutter.git"
            # Cloning resources
            Invoke-Expression "git clone https://gitlab.com/yukiz/ ./downloads/" # TODO : Find repo
            # Copy ressources to dir
            Copy-Item ".\"
        }
    }
    else { #If not okay, ask the user to install stuff manually
        Write-Host 'Please setup grasscutter manually by following the instructions at https://github.com/Grasscutters/Grasscutter/wiki/Running#starting-the-server and relaunch gumcutter and select yes at "Is grasscutter installed ?'
        Exit
    }
}