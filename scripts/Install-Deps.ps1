# Check for privileges
#Requires -RunAsAdministrator

# Parameter to pass the location of gumcutter
Param($WorkDir)

<#
    Program installation function doc : 
        - Applies to Git, Java and MongoDB
    If the software isn't installed, we download the setup.
    We then compute the hash and compare with an hardcoded one.
    If the hashes matches, we proceed with the installation, which happens silently for no user interaction.
    We wait for installation to complete before 
    In the case the hashes doesn't match, we error the installation.
#>

function Git {
    if ($null -eq (Get-Package "Git*" -ErrorAction SilentlyContinue)) {
        Write-Host "Downloading Git..."
        Invoke-WebRequest -Uri 'https://github.com/git-for-windows/git/releases/download/v2.40.0.windows.1/Git-2.40.0-64-bit.exe' -UseBasicParsing -OutFile "./downloads/git-setup.exe"
        if ((Get-FileHash "./downloads/git-setup.exe").Hash -eq "ff8954afb29814821e9e3759a761bdac49186085e916fa354bf8706e3c7fe7a2".ToUpper()) {
            Write-Host "Installing Git..."
            Start-Process ./downloads/git-setup.exe -ArgumentList '/silent /verysilent' -Wait
        } else {
            Throw "Hashes not matching. The downloaded file might be corrupted or someone might have tampered with it."
            Pause
            Exit 1
        }
    } else {
        Write-Host 'Git already installed, skipping...'
    }
}

function Java {
    if ($null -eq (Get-Package "Java(TM) SE Development Kit 17.*" -ErrorAction SilentlyContinue)) {
        Write-Host "Downloading JDK 17..."
        Invoke-WebRequest -Uri 'https://download.oracle.com/java/17/archive/jdk-17.0.7_windows-x64_bin.msi' -UseBasicParsing -OutFile "./downloads/jdk-17.msi"
        if ((Get-FileHash "./downloads/jdk-17.msi").Hash -eq "1a083e630c5f6f647d9542dccf26044bc67c850065fa9bb468e0f6bbd50f19a5".ToUpper()) {
            Write-Host "Installing JDK 17..."
            Start-Process ./downloads/jdk-17.msi -ArgumentList '/quiet' -Wait
        } else {
            Throw "Hashes not matching. The downloaded file might be corrupted or someone might have tampered with it."
            Pause
            Exit 1
        }
    } else {
        Write-Host 'JDK 17 already installed, skipping...'
    }
}

function MongoDB {
    if ($null -eq (Get-Package "MongoDB*" -ErrorAction SilentlyContinue)) {
        Write-Host "Downloading MongoDB..."
        Invoke-WebRequest -Uri 'https://fastdl.mongodb.org/windows/mongodb-windows-x86_64-6.0.5-signed.msi' -UseBasicParsing -OutFile "./downloads/mongodb.msi"
        if ((Get-FileHash "./downloads/mongodb.msi").Hash -eq "79327A9901A39182DEE2D74F84E46B4C6B1416CFC2C0CEE791322EA82DCE0388".ToUpper()) { # TODO : Check if correct hash
            Write-Host "Installing MongoDB..."
            Start-Process ./downloads/mongodb.msi -ArgumentList '/quiet' -Wait
        } else {
            Throw "Hashes not matching. The downloaded file might be corrupted or someone might have tampered with it."
            Pause
            Exit 1
        }
    } else {
        Write-Host 'MongoDB already installed, skipping...'
    }
}

function Cultivation {
    if ($null -eq (Get-Package "Cultivation*" -ErrorAction SilentlyContinue)) {
        Write-Host "Downloading Cultivation..."
        Invoke-WebRequest -Uri 'https://fastdl.mongodb.org/windows/mongodb-windows-x86_64-6.0.5-signed.msi' -UseBasicParsing -OutFile "./downloads/cultivation.msi"
        if ((Get-FileHash "./downloads/cultivation.msi").Hash -eq "79327A9901A39182DEE2D74F84E46B4C6B1416CFC2C0CEE791322EA82DCE0388".ToUpper()) { # TODO : Find the hash
            Write-Host "Installing MongoDB..."
            Start-Process ./downloads/cultivation.msi -ArgumentList '/quiet' -Wait
        } else {
            Throw "Hashes not matching. The downloaded file might be corrupted or someone might have tampered with it."
            Pause
            Exit 1
        }
    } else {
        Write-Host 'MongoDB already installed, skipping...'
    }
}

# We add gum to path, go into gumcutter folder and create config object
$env:Path += ";$WorkDir\bin\"
Set-Location $WorkDir


# We create a download folder if not present
if (-Not (Test-Path './downloads')) {
    New-Item -ItemType Directory -Name "downloads" > Out-Null
}

# Ask if user want to install Git (Optional but not recomended)
Write-Host 'Do you want to install Git? Installing Git will enable the update feature.'
$InstallGit = Invoke-Expression "gum choose 'Yes' 'No' 'Cancel installation'"

if ($InstallGit -eq "Yes") {
    # Ask user for confirmation
    Write-Host 'This method will install Git, JDK 17, MongoDB, Cultivation and the server. Proceed ?'
    Invoke-Expression "gum confirm 'Proceed ?' --default='No'"
    # If confirmed, proceed
    if ($LASTEXITCODE -eq 0) {
        Git
        Java
        MongoDB
        Cultivation
        Exit
    } else { # Else we abort
        Write-Host "Aborting installation. Bye !"
        Pause
        Exit
    }
} elseif ($InstallGit -eq "No") {
    # Ask user for confirmation
    Write-Host 'This method will install JDK 17, MongoDB, Cultivation and the server. Proceed ?'
    Invoke-Expression "gum confirm 'Proceed ?' --default='No'"
    # If confirmed, proceed
    if ($LASTEXITCODE -eq 0) {
        Java
        MongoDB
        Cultivation
        Write-Host "Successfuly installed dependencies!"
        Pause
        Exit
    } else { # Else we abort
        Write-Host "Aborting installation. Bye !"
        Pause
        Exit
    }
} elseif ($InstallGit = 'Cancel installation') {
    Write-Host "Aborting installation. Bye !"
    Pause
    Exit
}